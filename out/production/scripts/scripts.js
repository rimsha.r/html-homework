function enlarge(p) {
    document.getElementById("myModal").style.display = "block";
    var modalImg = document.getElementById("modalImg");
    modalImg.src = p.src;
}


function closeWindow() {
    document.getElementById("myModal").style.display = "none";
}

function shorten() {
    var after = 500;
    var paragraphs = document.getElementsByClassName("shorten");

    for (var i = 0; i < paragraphs.length; i++) {
        var content = paragraphs[i].innerHTML;
        paragraphs[i].innerHTML = content.substring(0, after) + "<span class='readmore' onclick = more(this)>...Read more</span>" + "<span class='expand'>" + content.substring(after) + "<span class='readless' onclick='less(this)'> Read less</span>" + "</span>";
    }
}

function more(p) {
    p.style.display = "none";
    var toHide = p.parentNode.childNodes[2];
    toHide.style.display = "inline";

}

function less(p) {
    var toHide = p.parentNode;
    var readmore = toHide.parentNode.childNodes[1];
    toHide.style.display = "none";
    readmore.style.display = "inline";
}